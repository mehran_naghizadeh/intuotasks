import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";

export default class Counter extends Component {
  @tracked A = '9123';
  @tracked B = '879123';

  // For this task, we don't know what the sum of two digits means. We just know how it is represented. 
  lookup = {
    '00': { carry: '0', sum: '0' },
    '01': { carry: '0', sum: '1' },
    '02': { carry: '0', sum: '2' },
    '03': { carry: '0', sum: '3' },
    '04': { carry: '0', sum: '4' },
    '05': { carry: '0', sum: '5' },
    '06': { carry: '0', sum: '6' },
    '07': { carry: '0', sum: '7' },
    '08': { carry: '0', sum: '8' },
    '09': { carry: '0', sum: '9' },

    '10': { carry: '0', sum: '1' },
    '11': { carry: '0', sum: '2' },
    '12': { carry: '0', sum: '3' },
    '13': { carry: '0', sum: '4' },
    '14': { carry: '0', sum: '5' },
    '15': { carry: '0', sum: '6' },
    '16': { carry: '0', sum: '7' },
    '17': { carry: '0', sum: '8' },
    '18': { carry: '0', sum: '9' },
    '19': { carry: '1', sum: '0' },

    '20': { carry: '0', sum: '2' },
    '21': { carry: '0', sum: '3' },
    '22': { carry: '0', sum: '4' },
    '23': { carry: '0', sum: '5' },
    '24': { carry: '0', sum: '6' },
    '25': { carry: '0', sum: '7' },
    '26': { carry: '0', sum: '8' },
    '27': { carry: '0', sum: '9' },
    '28': { carry: '1', sum: '0' },
    '29': { carry: '1', sum: '1' },

    '30': { carry: '0', sum: '3' },
    '31': { carry: '0', sum: '4' },
    '32': { carry: '0', sum: '5' },
    '33': { carry: '0', sum: '6' },
    '34': { carry: '0', sum: '7' },
    '35': { carry: '0', sum: '8' },
    '36': { carry: '0', sum: '9' },
    '37': { carry: '1', sum: '0' },
    '38': { carry: '1', sum: '1' },
    '39': { carry: '1', sum: '2' },

    '40': { carry: '0', sum: '4' },
    '41': { carry: '0', sum: '5' },
    '42': { carry: '0', sum: '6' },
    '43': { carry: '0', sum: '7' },
    '44': { carry: '0', sum: '8' },
    '45': { carry: '0', sum: '9' },
    '46': { carry: '1', sum: '0' },
    '47': { carry: '1', sum: '1' },
    '48': { carry: '1', sum: '2' },
    '49': { carry: '1', sum: '3' },

    '50': { carry: '0', sum: '5' },
    '51': { carry: '0', sum: '6' },
    '52': { carry: '0', sum: '7' },
    '53': { carry: '0', sum: '8' },
    '54': { carry: '0', sum: '9' },
    '55': { carry: '1', sum: '0' },
    '56': { carry: '1', sum: '1' },
    '57': { carry: '1', sum: '2' },
    '58': { carry: '1', sum: '3' },
    '59': { carry: '1', sum: '4' },

    '60': { carry: '0', sum: '6' },
    '61': { carry: '0', sum: '7' },
    '62': { carry: '0', sum: '8' },
    '63': { carry: '0', sum: '9' },
    '64': { carry: '1', sum: '0' },
    '65': { carry: '1', sum: '1' },
    '66': { carry: '1', sum: '2' },
    '67': { carry: '1', sum: '3' },
    '68': { carry: '1', sum: '4' },
    '69': { carry: '1', sum: '5' },

    '70': { carry: '0', sum: '7' },
    '71': { carry: '0', sum: '8' },
    '72': { carry: '0', sum: '9' },
    '73': { carry: '1', sum: '0' },
    '74': { carry: '1', sum: '1' },
    '75': { carry: '1', sum: '2' },
    '76': { carry: '1', sum: '3' },
    '77': { carry: '1', sum: '4' },
    '78': { carry: '1', sum: '5' },
    '79': { carry: '1', sum: '6' },

    '80': { carry: '0', sum: '8' },
    '81': { carry: '0', sum: '9' },
    '82': { carry: '1', sum: '0' },
    '83': { carry: '1', sum: '1' },
    '84': { carry: '1', sum: '2' },
    '85': { carry: '1', sum: '3' },
    '86': { carry: '1', sum: '4' },
    '87': { carry: '1', sum: '5' },
    '88': { carry: '1', sum: '6' },
    '89': { carry: '1', sum: '7' },

    '90': { carry: '0', sum: '9' },
    '91': { carry: '1', sum: '0' },
    '92': { carry: '1', sum: '1' },
    '93': { carry: '1', sum: '2' },
    '94': { carry: '1', sum: '3' },
    '95': { carry: '1', sum: '4' },
    '96': { carry: '1', sum: '5' },
    '97': { carry: '1', sum: '6' },
    '98': { carry: '1', sum: '7' },
    '99': { carry: '1', sum: '8' },
  };

  get solution() {
    if (this.error) {
      return '';
    }
    else {
      const arrayA = Array.from(this.A);
      const arrayB = Array.from(this.B);

      let carry = '0';
      let result = '';

      // While there are still digits
      while(arrayA.length || arrayB.length) {
        // Take the right-most digit from each number, or 0 if not available
        const a = arrayA.pop() || '0';
        const b = arrayB.pop() || '0';

        // Add the first digit to the carry
        const aPlusCarry = this.lookup[`${a}${carry}`];
        // Add the second digit to the 'sum' of previous operation
        const bPlusAPlusCarry = this.lookup[`${b}${aPlusCarry.sum}`];

        // Combine the 'carry' of the two previous operations
        carry = this.lookup[`${bPlusAPlusCarry.carry}${aPlusCarry.carry}`].sum;

        // Ass the 'sum' of bPlusAPlusCarry to the left of result
        result = bPlusAPlusCarry.sum + result;
      }

      // At the end, if 'carry' is not empty, add '1' to the left of result
      result = (carry == '1' ? '1' : '')  + result;

      return result;
    }
  }

  get error() {
    return isNaN(this.A) || isNaN(this.B);
  }
}
