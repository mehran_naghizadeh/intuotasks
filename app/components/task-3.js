import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";

export default class Task3 extends Component {
  @tracked input = 'middle-Outz';
  /**
   * Number of rotations.
   */
  @tracked k = 2;

  get solution() {
    let k = parseInt(this.k);
    k = Math.max(0, Math.min(100, k));

    return Array.from(this.input)
      .map(char => {
        const code = char.charCodeAt(0);
        if('a' <= char && char <= 'z') {
          const a = 'a'.charCodeAt(0);
          return String.fromCharCode(a + (code - a + k) % 26);
        }
        else if ('A' <= char && char <= 'Z') {
          const A = 'A'.charCodeAt(0);
          return String.fromCharCode(A + (code - A + k) % 26);
        }
        else {
          return char;
        }
      })
      .join('');
  }

  get error() {
    return this.k < 0;
  }
}