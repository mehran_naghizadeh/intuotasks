import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('tasks', function() {
    this.route('1');
    this.route('2');
    this.route('3');
    this.route('4');
  });
});
