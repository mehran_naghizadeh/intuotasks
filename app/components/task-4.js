import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';

export default class Task3 extends Component {
  @tracked word = 'BOOKKEEPER';
  @tracked startDate = 0;
  @tracked finishDate = 0;

  /**
   * For faster results, we use a lookup table to store factorils
   */
  factorilLookup = { '1': 1 };

  get sortedLetters() {
    const letters = [...new Set(Array.from(this.word))];
    return letters.sort();
  }

  /**
   * The soluiton is obtained by calculating the number of strings with a rank lower than ours
   */
  get solution() {
    // Keep track of execution time
    this.startDate = new Date();
    
    // Smallers are all the permutations that would be listed before this.word
    let smallersCnt = 0;
    for(let index = this.word.length - 2; index >= 0; --index) {
      // Fix all the letters up (index - 1) and calculate the number of possible permutations of the rest
      const trail = this.word.substring(index);
      smallersCnt += this.smallersCnt(trail);
    }
    
    // Keep track of execution time
    this.finishDate = new Date();

    // We have calculated the number of strings that come before this string. Rank is our ordinal number, i.e the calculated number plus one. 
    return smallersCnt + 1;
  }

  /**
   * Returns the number of permutaions of a string that would appear before it in a ASC sorted list of all permutations
   * @param {String} string 
   */
  smallersCnt(string) {
    if (string.length == 2) {
      return string.charAt(0) > string.charAt(1) ? 1 : 0;
    }
    else {
      let num = 0;
      const head = string.charAt(0);
      const tail = string.substring(1);
      this.sortedLetters
        .filter(char => char < string.charAt(0) && tail.includes(char))
        .forEach(char => {
          num += this.permutations( Array.from(tail.replace(char, head)) );
        });
      return num;
    }
  }

  /**
   * The number of possible permutations of the items of an array
   * @param {Array} array 
   */
  permutations(array) {
    // Let's suppose that the array consists of n elements, three of which are repeated a, b, c times, respectively, while the rest of items are not repeated. The number of permutations is then calculated as: n! / (a! b! c!)
    const repetitions = this.categorize(array)
    const result = this.factoril(array.length) / Object.values(repetitions).reduce((mult, item) => mult * this.factoril(item), 1);
    return result;
  }

  /**
   * This function takes an array and returns an object with distinct elements of the array as its keys and their frequencies in the array as its value
   * 
   * @param {Array} array 
   */
  categorize(array) {
    let arr = JSON.parse(JSON.stringify(array));
    const repetitions = {};

    while(arr.length) {
      const originalLength = arr.length
      const char = arr.shift();
      arr = arr.filter(e => e != char);
      repetitions[char] = originalLength - arr.length;
    }

    return repetitions;
  }

  factoril(n) {
    let value = this.factorilLookup[`${n}`];

    // If the result is not know, calculate it
    if (!value) {
      // The last known before n
      const m = Object.keys(this.factorilLookup)
        .filter(k => this.factorilLookup[`${k}`] && parseInt(k) < n)
        .reduce((max, item) => Math.max(max, parseInt(item)), 0);

      // Perform n x (n-1) x ... x m!
      value = Array(n - m).fill().map((_, index) => index + m + 1).reduce((f, item) => f * item, 1);
      value *= this.factorilLookup[`${m}`];

      // Store the result in the lookup table
      this.factorilLookup[`${n}`] = value;
    }

    return value;
  }

  /**
   * The time it took for the calculation to complete
   */
  get elapsedTime() {
    return this.finishDate.getTime() - this.startDate.getTime();
  }

  /**
   * This indicates when the calculation started
   */
  get startTime() {
    return this.startDate.getTime();
  }
  
  
    /**
     * This indicates when the calculation finished
     */
  get finishTime() {
    return this.finishDate.getTime();
  }
}
