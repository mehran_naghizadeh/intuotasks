import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";

export default class Counter extends Component {
  @tracked n = 5;

  RIGHT = 'Rip the top row from left to right';
  DOWN = 'Rip the right column from top to bottom';
  LEFT = 'Rip the bottom column from right to left';
  UP = 'Rip the left column from bottom to top';

  directions = [ this.RIGHT, this.DOWN, this.LEFT, this.UP];

  /**
   * Create the nxn array filled by numbers 1, 2, 3, ..., nxn
   */
  get array() {
    // Avoid array n < 1 or n == ""
    const m = Math.max(1, parseInt(this.n) || 1);
    return Array(m).fill().map((_, index) => Array(m).fill().map((_, i) => m * index + i + 1));
  }

  /**
   * The solution is obtained by successively ripping the array off.
   * After each rip, the direction is changed.
   */
  get solution() {
    let directionIndex = 0;
    let result = [];

    let array = JSON.parse(JSON.stringify(this.array));
    while(array.length) {
      result = result.concat(this.rip(array, this.directions[directionIndex]));
      directionIndex = (directionIndex + 1) % this.directions.length;
    }

    return result;
  }

  /**
   * Rip the array in a certain direction.
   * This function returns the ripped column or row and removes the ripped entries from the original array.
   * 
   * @param {Array} array 
   * @param {String} direction 
   */
  rip(arr, direction) {
    // let arr = JSON.parse(JSON.stringify(array));
    let rip = [];
    switch(direction) {
      case this.RIGHT:
        // Ripping to the right happens only on the top row, i.e. the first element of the multi-dimensional array
        rip = arr.shift();
        break;
        
        case this.LEFT:
        // Ripping to the left happens only on the bottom row, i.e. the last element of the multi-dimensional array. It rips the row from right to left, hence we should reverse.
        rip = arr.pop().reverse();
        break;

      case this.DOWN:
        // Ripping down happens only in the right column. This column consists of the last elemen of each element(/array) of the mutli-dimensional array.
        rip = arr.map(a => a.pop());
        break;

      case this.UP:
        // Ripping up happens only in the left column. This column consists of the first elemen of each element(/array) of the mutli-dimensional array. It rips the column from bottom to tp, hence we should reverse.
        rip = arr.map(a => a.shift()).reverse();
        break;
      default:
        break;
    }

    return rip;
  }

  get error() {
    return isNaN(this.n) || parseInt(this.n) < 1;
  }
}